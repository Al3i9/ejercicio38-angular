import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DatosComponent } from './components/datos/datos.component';

const routes: Routes = [
  { path: 'datos', component: DatosComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'datos' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
