import { Component, OnInit } from '@angular/core';
import { Ipersona } from 'src/app/interface/persona.interface';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.css']
})
export class DatosComponent implements OnInit {


  informacion: Ipersona = {
    nombres:'',
    direccion:''
  }
  constructor() { }

  ngOnInit(): void {
  }

  guardar():void {
    console.log(this.informacion.nombres);
    console.log(this.informacion.direccion);
    
  }

}
